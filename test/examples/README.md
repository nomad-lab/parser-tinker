# Tests:
### Source of Tests:

The tests are taken from Tinker (version 8.1) examples in [TINKER](http://dasher.wustl.edu/tinker/).

### TEST Directory for the TINKER Package

This directory contains some test casess illustrating use of
several of the programs in the TINKER package.

For example, the argon.run file contains the commands to run
the first test case. The resulting output is found in the file
argon.log.

#### ANION       
estimation of the free energy of hydration
of Cl- anion vs. Br- anion via a 2 picosecond
simulation on a "hybrid" anion in a box of
water, followed by free energy perturbation

#### ARGON
minimization followed by 6 picoseconds
of a molecular dynamics run on a periodic
box containing 150 argon atoms

#### ENKEPHALIN
generation of coordinates from the
amino acid sequence and phi/psi angles,
followed by energy minimization and
determination of the lowest frequency
normal mode

#### ICE/WATER
short MD simulation of the monoclinic ice V
crystal form using the iAMOEBA water model,
neighbor lists and PME electrostatics

